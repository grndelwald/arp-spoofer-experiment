#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/ip_icmp.h>
#include<linux/if_ether.h>
#include<net/ethernet.h>
#include<linux/if_packet.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<netinet/in.h>
#include<netinet/in.h>
#include<netdb.h>
#include<errno.h>

uint16_t checksum(uint16_t *buffer,int size)
{
	uint32_t sum =0;
	while(size>1)
	{
		sum += *(unsigned short*)buffer++;
		size -= 2;
	}
	if(size>0)
		sum += *(unsigned char *)buffer;
	sum = (sum & 0xffff)+(sum >>16);
	sum += (sum>>16);
	return (uint16_t)(~sum);
}
char mac[6];

void *getmac(char *host,char *target)
{
	int sockfd,packet;
	char  buffer[8192];
	memset(buffer,0,sizeof(buffer));
	struct icmphdr *icmp;
	struct iphdr *ip = (struct iphdr*)buffer;
	struct sockaddr_in sock;
	struct sockaddr_ll device;
	int addrlen;
	bzero(&sock,sizeof(sock));
	icmp = (struct icmphdr*)(buffer+sizeof(struct iphdr));
	if((sockfd = socket(AF_INET,SOCK_RAW,IPPROTO_ICMP))<0)
	{
		perror("socket");
		exit(0);
	}
	ip->ihl = 5;
	ip->version = 4;
	ip->tot_len = sizeof(struct iphdr)+sizeof(struct icmphdr);
	ip->tos = 0;
	ip->id = htons(1);
	ip->ttl = 64;
	ip->check=0;
	ip->protocol = IPPROTO_ICMP;
	ip->saddr = inet_addr(host);
	ip->daddr = inet_addr(target);
	icmp->type = 8;
	icmp->code = htons(0);
	icmp->un.echo.id = htons(13456);
	icmp->un.echo.sequence = htons(1);
	icmp->checksum = (uint16_t)checksum(icmp,sizeof(struct icmphdr));
	ip->check = (uint16_t)checksum(ip,sizeof(struct iphdr)+sizeof(struct icmphdr));
	int opt = 1;
	sock.sin_family = AF_INET;
	sock.sin_addr.s_addr = inet_addr(target);
	if(setsockopt(sockfd,IPPROTO_IP,IP_HDRINCL,(void*)&opt,sizeof(opt))<0)
	{
		perror("setsockopt");
		return 0;
	}
	if(sendto(sockfd,ip,ip->tot_len,0,&sock,sizeof(sock))<0)
	{
		perror("sendto");
		return 0;
	}
	if((packet = socket(AF_PACKET,SOCK_RAW,htons(ETH_P_IP)))<0)
	{
		perror("socket");
		return 1;
	}
	device.sll_family = AF_PACKET;
	device.sll_protocol = htons(1);
	device.sll_halen = htons(6);
	device.sll_pkttype = htons(PACKET_HOST);
	device.sll_hatype  = htons(ETHERTYPE_ARP);
	device.sll_ifindex = 3;
	addrlen = sizeof(device);
	while(1)
	{
		char reply[8192];
		int addrlen = sizeof(sock);
		if(recvfrom(packet,reply,sizeof(reply),0,&device,&addrlen)>0)
		{
			struct iphdr *iph = (struct iphdr*)(reply+sizeof(struct ether_header));
			if(ip->protocol == IPPROTO_ICMP)
			{
				struct ether_header * eth = (struct ether_header *)reply;
				memcpy(&mac,&eth->ether_shost,6);
				break;
			}
		}
	}
	return (void*)mac;
}

